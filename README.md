# README #

Holds build scripts for PureDNS project

### What is this repository for? ###

* Central location for all CI/CD scripts
* 1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Dependencies
   * Ansible 2.3.1
   * yum-utils, device-mapper-persistent-data, lvm2, python-pip
   * Docker CE 17.03.1
   
### Contribution guidelines ###

* Please send in PR to hammad.shah@zigron.com for review 

### Who do I talk to? ###

* hammad.shah@zigron.com
* umair.shabir@zigron.com
